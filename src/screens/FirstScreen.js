import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image
} from "react-native";
import moment from "moment";

// redux
import { connect } from "react-redux";

// Actions
import {
  addFromThunk,
  remoteValue,
  authSingIn,
  getPatientData,
  getPing,
  getAppointments
} from "../store/actions";

class FirstScreen extends Component {
  constructor() {
    super();
    this.state = {
      startTime: null,
      calculateTime: null
    };
  }

  initTimer = () => {
    this.setState({ startTime: moment() }, () => {
      setInterval(() => {
        this.setState({
          calculateTime: moment(moment() - moment(this.state.startTime)).format(
            "mm:ss"
          )
        });
      }, 1000);
    });
  };

  render() {
    const {
      addFromThunk,
      remoteValue,
      list,
      authSingIn,
      isAuth,
      getPatientData,
      getPing,
      ping,
      getAppointments
    } = this.props;

    const { calculateTime } = this.state;

    return (
      <ScrollView style={styles.container}>
        <View
          style={{
            paddingVertical: 20,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            style={{
              width: "80%",
              backgroundColor: "gray",
              paddingVertical: 10,
              paddingHorizontal: 10,
              borderRadius: 10,
              marginTop: 30
            }}
            onPress={() => {
              this.initTimer();
              authSingIn();
            }}
          >
            <Text style={{ color: "white", fontSize: 18 }}>Login</Text>
          </TouchableOpacity>

          {calculateTime && (
            <View style={{ marginVertical: 10, flexDirection: "row" }}>
              <Text style={{ color: "#ddf" }}>Ping result -</Text>
              <Text style={{ color: "#ddf", width: 50, paddingLeft: 10 }}>
                {calculateTime}
              </Text>
            </View>
          )}

          {isAuth && (
            <TouchableOpacity
              style={{
                width: "80%",
                backgroundColor: "#99cc55",
                paddingVertical: 10,
                paddingHorizontal: 10,
                borderRadius: 10
              }}
              onPress={() => {
                getAppointments();
                getPatientData();
                getPing();
              }}
            >
              <Text style={{ color: "white", fontSize: 18 }}>
                Send couple requests
              </Text>
            </TouchableOpacity>
          )}
        </View>

        {ping && (
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <Text style={{ color: "#ddf" }}>Token was got -</Text>
            <Text style={{ color: "#ddf", width: 150, paddingLeft: 10 }}>
              {ping}
            </Text>
          </View>
        )}

        <View>
          <FlatList
            style={styles.list}
            data={list}
            renderItem={({ item }) => (
              <View style={styles.item}>
                {/* <Image
                  style={{ width: 50, height: 50 }}
                  source={{
                    uri: item
                  }}
                /> */}
                {/* <TouchableOpacity
                  style={styles.remove}
                  onPress={() => remoteValue(item)}
                >
                  <Text style={{ color: "white" }}>REMOVE</Text>
                </TouchableOpacity> */}
                <Text style={{ color: "white" }}>{item.id}</Text>
              </View>
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#2c3e50"
  },
  list: {
    paddingHorizontal: 20,
    marginTop: 20
  },
  item: {
    backgroundColor: "#5fba7d",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 5,
    justifyContent: "space-between"
  },
  remove: {
    backgroundColor: "#ff567d",
    alignItems: "center",
    padding: 8,
    borderRadius: 4
  }
});

const mapStateToProps = state => ({
  list: state.appointments,
  isAuth: state.auth.isAuth,
  ping: state.ping
});

export default connect(
  mapStateToProps,
  {
    addFromThunk,
    remoteValue,
    authSingIn,
    getPatientData,
    getPing,
    getAppointments
  }
)(FirstScreen);
