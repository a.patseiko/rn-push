import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import Centrifuge from "centrifuge";

export default class ChatScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messaging: [],
      clientId: null,
      inputValue: null,
      centrifuge: null,
      subscription: null
    };
  }

  componentDidMount() {
    console.log("DID mount");
  }

  connect = () => {
    const { clientId } = this.state;

    const callbacks = {
      publish: res => {
        // See below description of message format
        console.log(res);
        this.updateMessages(res);
      },
      join: message => {
        // See below description of join message format
        console.log("join ", message);
      },
      leave: message => {
        // See below description of leave message format
        console.log("leave ", message);
      },
      subscribe: context => {
        // See below description of subscribe callback context format
        console.log("subscribe ", context);
      },
      error: errContext => {
        // See below description of subscribe error callback context format
        console.log("error ", err);
      },
      unsubscribe: context => {
        // See below description of unsubscribe event callback context format
        console.log("unsubscribe ", context);
      }
    };

    if (!clientId) {
      const centrifuge = new Centrifuge(
        "ws://192.168.12.80:8000/connection/websocket"
      );
      this.setState({ centrifuge });

      // Subscribe on chanel
      const subscription = centrifuge.subscribe("news", callbacks);

      subscription.history().then(
        message => {
          if (message && Array.isArray(message.publications)) {
            console.log(message.publications);
            this.setState({ messaging: message.publications });
          }
        },
        err => {
          console.log("History err", err);
        }
      );

      subscription.presence().then(
        res => {
          console.log("presence ", res);
          // presence data received
        },
        err => {
          console.log("presence err ", err);
          // presence call failed with error
        }
      );

      centrifuge.on("connect", context => {
        // now client connected to Centrifugo and authorized
        console.log("========== Connect =========== ", context);
        this.setState({ clientId: context.client, subscription: subscription });
      });

      centrifuge.on("disconnect", context => {
        console.log("DISCONECT ", context);
        this.setState({
          clientId: null,
          inputValue: "",
          centrifuge: null,
          subscription: null
        });
      });

      centrifuge.connect();
    }
  };

  updateMessages = message => {
    const { messaging } = this.state;
    const msg = [...messaging, message];
    this.setState({ messaging: msg });
  };

  sendMessage = () => {
    const { inputValue, subscription, clientId } = this.state;
    if (clientId && inputValue) {
      subscription.publish(inputValue).then(
        res => {
          this.setState({ inputValue: "" });
        },
        err => {
          // publish call failed with error
        }
      );
    }
  };

  disconnect = () => {
    const { centrifuge } = this.state;
    centrifuge.disconnect();
  };

  render() {
    const { messaging, clientId, inputValue } = this.state;
    const { height } = Dimensions.get("window");
    const isMyMessages = msg => msg && msg.info && msg.info.client === clientId;

    return (
      <KeyboardAvoidingView
        style={{ flex: 1, backgroundColor: "#2c3e50" }}
        behavior="padding"
        enabled
        keyboardVerticalOffset={-Math.round(height / 2)}
      >
        <View style={[styles.container]}>
          <View style={styles.btnsContainer}>
            <TouchableOpacity
              style={[styles.btn, clientId && { backgroundColor: "gray" }]}
              onPress={this.connect}
            >
              <Text>Connect</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.btn,
                { backgroundColor: clientId ? "#ff567d" : "gray" }
              ]}
              onPress={this.disconnect}
            >
              <Text>Disconnect</Text>
            </TouchableOpacity>
          </View>

          <ScrollView
            style={styles.msgBlock}
            keyboardShouldPersistTaps="handled"
            ref="scrollView"
            onContentSizeChange={(width, height) =>
              this.refs.scrollView.scrollTo({ y: height })
            }
          >
            {messaging.map((el, i) =>
              el && typeof el.data === "string" ? (
                <View
                  key={i}
                  style={[
                    { flexDirection: "row" },
                    isMyMessages(el) && {
                      justifyContent: "flex-end"
                    }
                  ]}
                >
                  <Text
                    style={[
                      styles.msg,
                      isMyMessages(el) && { backgroundColor: "#66cc" }
                    ]}
                  >
                    {el.data}
                  </Text>
                </View>
              ) : null
            )}
          </ScrollView>

          <View style={styles.inputMsgWrapper}>
            <TextInput
              style={styles.inputText}
              onChangeText={inputValue => this.setState({ inputValue })}
              value={inputValue}
            />
            <TouchableOpacity
              style={[
                styles.btn,
                { width: 50 },
                !clientId && inputValue && { backgroundColor: "gray" }
              ]}
              onPress={this.sendMessage}
            >
              <Text>Go</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    paddingBottom: 10,
    paddingTop: 30,
    flex: 1,
    flexDirection: "column"
  },
  btnsContainer: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  btn: {
    backgroundColor: "#5fba7d",
    alignItems: "center",
    padding: 8,
    borderRadius: 4,
    width: "40%",
    justifyContent: "center"
  },
  msgBlock: {
    flex: 1,
    paddingHorizontal: 20,
    marginBottom: 10,
    marginTop: 10
  },
  inputText: {
    flex: 1,
    backgroundColor: "gray",
    height: 40,
    padding: 10,
    borderRadius: 5,
    color: "white",
    marginRight: 10
  },
  inputMsgWrapper: {
    flexDirection: "row",
    width: "100%"
  },
  msg: {
    padding: 5,
    borderRadius: 5,
    backgroundColor: "#3ccc",
    color: "white",
    marginBottom: 5
  }
});
