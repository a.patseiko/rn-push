import axios from "axios";
import store from "./store";

// Actions type
export const ADD = "ADD";
export const REMOTE = "REMOTE";

export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const SET_PING = "SET_PING";

export const APPOINTMENTS_SET = "APPOINTMENTS_SET";
export const REFRESHING_TOKEN = "REFRESHING_TOKEN";
export const DONE_REFRESHING_TOKEN = "DONE_REFRESHING_TOKEN";

// Actions
export const addValue = value => ({
  type: ADD,
  payload: value
});

export const remoteValue = value => ({
  type: REMOTE,
  payload: value
});

export const addFromThunk = init => (dispatch, getState) => {
  axios
    .get("https://dog.ceo/api/breeds/image/random", {
      params: { actionName: "addValue", actionParams: init }
    })
    .then(({ data: { message } }) => {
      dispatch(addValue(message));
    });
};

export const loginSuccess = tokens => ({
  type: LOGIN_SUCCESS,
  payload: { ...tokens }
});

export const authSingIn = () => dispatch => {
  axios({
    method: "post",
    url: "http://auth.umbi-dev.inside.cactussoft.biz/v1.2/auth/login",
    data: { email: "andrey.patseyko@cactussoft.biz", password: "111111Aa" }
  }).then(({ data }) => {
    dispatch(loginSuccess(data));
  });
};

export const getPatientData = () => (dispatch, getState) => {
  const {
    auth: { access_token }
  } = getState();
  axios({
    method: "get",
    url: "http://auth.umbi-dev.inside.cactussoft.biz/v1.2/patients/me",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
    .then(res => {
      console.log("getPatientData SUCCESS");
    })
    .catch(err => {
      console.log("getPatientData ERR ", err);
    });
};

export const getPing = () => (dispatch, getState) => {
  const {
    auth: { access_token }
  } = getState();
  axios({
    method: "get",
    url: "http://auth.umbi-dev.inside.cactussoft.biz/v1.2/auth/ping",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
    .then(res => {
      dispatch({ type: SET_PING, payload: (Math.random() * 1000).toFixed(2) });
      console.log("getPing SUCCESS");
    })
    .catch(err => {
      console.log("getPing ERR ", err);
    });
};

export const getAppointments = () => (dispatch, getState) => {
  const {
    auth: { access_token }
  } = getState();
  axios({
    method: "get",
    url: "http://booking.umbi-dev.inside.cactussoft.biz/v1.2/bookings/nearest",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
    .then(({ data }) => {
      console.log("getAppointments SUCCESS");
      dispatch({
        type: APPOINTMENTS_SET,
        payload: data.appointments
      });
    })
    .catch(err => {
      console.log("Err ", err);
    });
};

export function refreshToken() {
  const data = { refreshToken: store.getState().auth.tokens.refresh_token };
  var freshTokenPromise = axios({
    method: "post",
    url: `http://auth.umbi-dev.inside.cactussoft.biz/v1.2/auth/refreshtoken`,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json; charset=utf-8"
    },
    data
  })
    .then(({ data }) => {
      store.dispatch({
        type: DONE_REFRESHING_TOKEN
      });

      store.dispatch(loginSuccess(data));

      return data
        ? Promise.resolve(data)
        : Promise.reject({
            message: "could not refresh token"
          });
    })
    .catch(e => {
      console.log("error refreshing token", e);

      store.dispatch(loginSuccess(null));
      store.dispatch({
        type: DONE_REFRESHING_TOKEN
      });
      return Promise.reject(e);
    });

  store.dispatch({
    type: REFRESHING_TOKEN,

    // we want to keep track of token promise in the state so that we don't try to refresh
    // the token again while refreshing is in process
    payload: freshTokenPromise
  });

  return freshTokenPromise;
}

axios.interceptors.response.use(config => {
  console.log("request config ", config.config.url);
  return config;
});
