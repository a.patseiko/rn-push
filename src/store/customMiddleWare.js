import jwtDecode from "jwt-decode";
import moment from "moment";
import { refreshToken } from "./actions";

export const customMiddleWare = ({ dispatch, getState }) => next => action => {
  // SOURCE http://qaru.site/questions/171562/how-to-use-redux-to-refresh-jwt-token
  // only worry about expiring token for async actions
  if (typeof action === "function") {
    if (getState().auth && getState().auth.access_token) {
      // decode jwt so that we know if and when it expires
      var tokenExpiration = Number(
        jwtDecode(getState().auth.access_token).exp + "000"
      );

      if (
        tokenExpiration &&
        moment(tokenExpiration) - moment(Date.now()) < 5000
      ) {
        console.log("HAS expired", getState().auth.freshTokenPromise);
        // make sure we are not already refreshing the token
        if (getState().auth.freshTokenPromise) {
          return getState().auth.freshTokenPromise.then(() => {
            console.log("RUN after finish promise");
            next(action);
          });
        } else {
          return refreshToken().then(() => next(action));
        }
      }
    }
  }

  return next(action);
};
