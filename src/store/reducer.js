import {
  ADD,
  REMOTE,
  LOGIN_SUCCESS,
  SET_PING,
  REFRESHING_TOKEN,
  DONE_REFRESHING_TOKEN,
  APPOINTMENTS_SET
} from "./actions";

const initState = {
  list: [],
  auth: {
    isAuth: false,
    freshTokenPromise: null
  },
  ping: null,
  appointments: null
};

export default (state = initState, action) => {
  switch (action.type) {
    case ADD: {
      const tList = [...state.list];
      tList.push(action.payload);
      return { ...state, list: tList };
    }
    case REMOTE: {
      const filterList = state.list.filter(el => action.payload !== el);
      return { ...state, list: filterList };
    }
    case LOGIN_SUCCESS: {
      const tAuth = { ...state.auth, ...action.payload };
      tAuth.isAuth = true;
      return { ...state, auth: tAuth };
    }
    case SET_PING: {
      return { ...state, ping: action.payload };
    }
    case REFRESHING_TOKEN: {
      const tAuth = { ...state.auth };
      tAuth.freshTokenPromise = action.payload;
      return { ...state, auth: tAuth };
    }
    case DONE_REFRESHING_TOKEN: {
      const tAuth = { ...state.auth };
      tAuth.freshTokenPromise = null;
      return { ...state, auth: tAuth };
    }
    case APPOINTMENTS_SET: {
      return { ...state, appointments: action.payload };
    }

    default:
      return state;
  }
};
