import reducer from "./reducer";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { customMiddleWare } from "./customMiddleWare";

const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(customMiddleWare, thunk))
);

export default store;
