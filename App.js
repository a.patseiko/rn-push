import React, { Component } from "react";
import { Text, View } from "react-native";
import { Provider } from "react-redux";
import store from "./src/store/store";

// Components
import FirstScreen from "./src/screens/FirstScreen";
import PushController from "./src/PushController";
import ChatScreen from "./src/screens/ChatScreen";

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Provider store={store}>
          <ChatScreen />
          {/* <View style={{ flex: 1 }}>
            <FirstScreen />
          </View> */}
        </Provider>

        {/* <PushController /> */}
      </View>
    );
  }
}
